use std::thread::{self, JoinHandle};
use std::sync::{Arc, Mutex};
use num::{Complex, complex::ComplexFloat};
use rand::Rng;
use raylib::prelude::*;



// Paint

fn paint() {
    let (mut rl, thread) = raylib::init()
        .size(1920, 1080)
        .title("Фрактал")
        .build();

    let mut d = rl.begin_drawing(&thread);
    d.clear_background(Color::WHITE);
    d.draw_text("Press LMB to draw", 10, 10, 18, Color::BLACK);
    d.draw_text("Press RMB to clear", 10, 30, 18, Color::BLACK);
    d.draw_text("Press MMB to toggle bg", 10, 50, 18, Color::BLACK);
    drop(d);

    let mut white_bg: bool = true;
    let mut prev_pos: Vector2 = Vector2::new(0.0, 0.0);
    while !rl.window_should_close() {
        let mut d = rl.begin_drawing(&thread);

        let pos = d.get_mouse_position();
        if d.is_mouse_button_down(MouseButton::MOUSE_LEFT_BUTTON) {
            if white_bg {
                d.draw_line_v(pos, prev_pos, Color::BLACK);
            }
            else {
                d.draw_line_v(pos, prev_pos, Color::WHITE);
            }
        }
        else if d.is_mouse_button_down(MouseButton::MOUSE_RIGHT_BUTTON) {
            if white_bg {
                d.clear_background(Color::WHITE);
            }
            else {
                d.clear_background(Color::BLACK);
            }
        } else if d.is_mouse_button_pressed(MouseButton::MOUSE_MIDDLE_BUTTON) {
            white_bg = !white_bg;
            if white_bg {
                d.clear_background(Color::WHITE);
            }
            else {
                d.clear_background(Color::BLACK);
            }
        }
        prev_pos = pos;
    }
}



// Треугольник серпинского

fn triagnle() {
    fn generate_triangle() -> Vec<Vector2> {
        let mut rng = rand::thread_rng();

        loop {
            let a = Vector2::new(rng.gen_range(0..1920) as f32, rng.gen_range(0..1080) as f32);
            let b = Vector2::new(rng.gen_range(0..1920) as f32, rng.gen_range(0..1080) as f32);
            let c = Vector2::new(rng.gen_range(0..1920) as f32, rng.gen_range(0..1080) as f32);
        
            let ab = a.distance_to(b);
            let bc = b.distance_to(c);
            let ca = c.distance_to(a);
        
            let area = (ab + bc + ca) / 2.0;
            if area > 2000.0 {
                println!("AREA: {}", area);
                return vec![a, b, c];
            }
        }
    }

    fn get_middle_point(a: Vector2, b: Vector2) -> Vector2 {
        Vector2::new((a.x + b.x) / 2.0, (a.y + b.y) / 2.0)
    }

    let (mut rl, thread) = raylib::init()
        .size(1920, 1080)
        .title("Фрактал")
        .build();

    rl.hide_cursor();

    let mut rng = rand::thread_rng();

    let mut anchors: Vec<Vector2> = generate_triangle();

    let mut d = rl.begin_drawing(&thread);
    d.clear_background(Color::BLACK);

    d.draw_text("Press LMB to regenerate", 10, 10, 18, Color::WHITE);

    for anchor in anchors.iter() {
        d.draw_pixel_v(anchor, Color::WHITE);
    }

    let mut point: Vector2 = get_middle_point(anchors[0], anchors[1]);
    d.draw_pixel_v(point, Color::WHITE);
    drop(d);
    
    while !rl.window_should_close() {
        let mut d = rl.begin_drawing(&thread);

        if d.is_mouse_button_pressed(MouseButton::MOUSE_LEFT_BUTTON) {
            d.clear_background(Color::BLACK);
            anchors = generate_triangle();
            point = get_middle_point(anchors[0], anchors[1]);
        }

        let anchor: usize = rng.gen_range(0..3);
        let anchor: Vector2 = anchors[anchor];

        let middle_point: Vector2 = get_middle_point(point, anchor);

        d.draw_pixel_v(middle_point, Color::WHITE);

        point = middle_point;
    }
}

const THREADS: i32 = 32;

fn julia(c: Complex<f64>, limit: f64, iterations: i32) {
    let w: i32 = 1000;
    let h: i32 = 1000;

    let (mut rl, thread) = raylib::init()
        .size(w, h)
        .title("Фрактал")
        .build();

    rl.hide_cursor();

    let half_w: i32 = w / 2;
    let half_h: i32 = h / 2;

    let scale_factor = 2.0;
    let scale_x: f64 = half_w as f64 / scale_factor;
    let scale_y: f64 = half_h as f64 / scale_factor;

    let degree: Arc<Mutex<f64>> = Arc::new(Mutex::new(2.0));

    while !rl.window_should_close() {
        let pixels: Arc<Mutex<Vec<Vector2>>> = Arc::new(Mutex::new(vec![]));
        let colors: Arc<Mutex<Vec<Color>>> = Arc::new(Mutex::new(vec![]));
        let mut threads: Vec<JoinHandle<()>> = vec![];

        for t in 0..THREADS {
            let pixels: Arc<Mutex<Vec<Vector2>>> = Arc::clone(&pixels);
            let colors: Arc<Mutex<Vec<Color>>> = Arc::clone(&colors);

            let mut pixels_local: Vec<Vector2> = vec![];
            let mut colors_local: Vec<Color> = vec![];

            let degree_guard = degree.lock().unwrap();
            let degree = *degree_guard;
            drop(degree_guard);

            let local_h_step = h / THREADS;

            threads.push(thread::spawn(move || {
                for ly in local_h_step * t..local_h_step * (t + 1) {
                    let y = ly - half_h;
                    let y_scaled: f64 = y as f64 / scale_y;
                    for x in -half_w..half_w {
                        let mut draw: bool = true;
                        let mut z: Complex<f64> = Complex::new(x as f64 / scale_x, y_scaled);
                        let mut z_abs: f64 = 0.0;
                        let mut iteration = 0;
                        while draw && iteration < iterations {
                            z = z.powf(degree) + c;
                            z_abs = z.abs();
                            draw = z_abs < limit;
                            iteration += 1;
                        }
                        if draw {
                            pixels_local.push(Vector2::new((half_w + x) as f32, (half_h + y) as f32));
                            colors_local.push(Color::color_from_hsv(222.0, 1.0, (z_abs / limit * 1.5) as f32));
                        }
                    }
                }

                let mut pixels_guard = pixels.lock().unwrap();
                let mut colors_guard = colors.lock().unwrap();

                pixels_guard.append(&mut pixels_local);
                colors_guard.append(&mut colors_local);
            }));
        }

        while threads.len() > 0 {
            let thread = threads.remove(0);
            thread.join().unwrap();
        }

        let mut d = rl.begin_drawing(&thread);
        d.clear_background(Color::BLACK);
        for (index, pixel) in pixels.lock().unwrap().iter().enumerate() {
            d.draw_pixel_v(pixel, colors.lock().unwrap()[index]);
        }
        d.draw_fps(10, 10);

        *degree.lock().unwrap() += 0.01;
    }
}

fn main() {
    paint();
    triagnle();
    julia(Complex::new(-0.74543, 0.11301), 1.50197192317588, 10);
    julia(Complex::new(0.251, 0.875), 1.50197192317588, 10);
}